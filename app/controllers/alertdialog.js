function handleReset() {
	//reset password
	Alloy.Globals.progress.setMessage(Ti.Locale.getString('checking_email'));
	Alloy.Globals.ajaxClient.sendData(Alloy.CFG.reset_password_path, {email: $.reset_email.getValue()}, 
		function(result) {
			
			if(result) {
				alert("Email sent with reset link.");
			} 
			
			Alloy.Globals.progress.hide();
			$.alertdialog.close();	
		}
	);
}
function closeWindow() {
	$.alertdialog.close();
}
exports.showDialog = function() {
	$.alertdialog.open();
};

//Hide keyboard when clicking outside of text box
$.alertdialog.addEventListener("click", function() {
	$.reset_email.blur();
}); 

$.reset_email.addEventListener("click", function(e) {
	e.cancelBubble = true;
	$.reset_email.focus();
});


