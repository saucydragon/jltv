//orientation fix
$.videolist.orientationModes=[Titanium.UI.PORTRAIT];

function viewVideo(e) {	

	//create videoview.xml with details based upon selected model
 	if (e.section.getItemAt(e.itemIndex).model) {  
 	  //obtain corresponding model from the video collection and pass it into the view
	  var detailObj=Alloy.Collections.videos.get(e.section.getItemAt(e.itemIndex).model.id);  
	  var win=Alloy.createController('videoview',{"$model":detailObj});  
	  win.getView().open();  
  } 
}

function navigateHome() {
	$.videolist.close();
	
	if (Alloy.Globals.homeScreenMgr == null) {
		Alloy.Globals.homeScreenMgr = Alloy.createController("homescreen");
	}
	
	Alloy.Globals.homeScreenMgr.getView().open();
}
/* FOR LOGIN FUNCTIONALITY
function Logout() {
	Alloy.Globals.sessionMgr.endSession();
	$.videolist.close();
	
	if (Alloy.Globals.loginMgr == null) {
		Alloy.Globals.loginMgr = Alloy.createController("loginpage");
	}
	
	Alloy.Globals.loginMgr.getView().open();
}
*/

exports.populateList = function() {

	if (Ti.Platform.osname == 'android'){
		Alloy.Globals.progress.setMessage(Ti.Locale.getString('loading_list'));  
	}
	
   //".fetch()" populates the collection via the structure/adapter specified within the video model
   var videoModels = Alloy.Collections.videos;
   videoModels.fetch({
   	 
	    success : function(model, resp) {
	      	$.videolist.open();
		},
		
	    error : function(model, resp) {

	        alert("Error loading video list. Please try again later.");
	        
	    }
	});
	
};

/*////////////////////
 *  WINDOW LISTENERS
 *////////////////////
$.videolist.addEventListener("open", function() {
	if(Alloy.Globals.progress) {
		try{ Alloy.Globals.progress.hide(); } catch(error) {};
	}
});
$.videolist.addEventListener("close", function() {
	//clean data binding to prevent memory leaks
});
$.videolist.addEventListener("android:back", function() {
	//"home" button functionality. Leave app runnning in background.
	var intent = Ti.Android.createIntent({
        action: Ti.Android.ACTION_MAIN
    });
    intent.addCategory(Ti.Android.CATEGORY_HOME);
    Ti.Android.currentActivity.startActivity(intent);
});