//orientation fix
$.homescreen.orientationModes=[Titanium.UI.PORTRAIT];

function navigateEpisodeList() {
	$.homescreen.close();
	
	if (Alloy.Globals.videoListMgr == null) {
		Alloy.Globals.videoListMgr = Alloy.createController("videolist");
		Alloy.Globals.videoListMgr.populateList();
	}
	
	Alloy.Globals.videoListMgr.getView().open();
}

/*////////////////////
 *  WINDOW LISTENERS
 *////////////////////
$.homescreen.addEventListener("open", function() {
	if(Alloy.Globals.progress) {
		try{ Alloy.Globals.progress.hide(); } catch(error) {};
	}
});
$.homescreen.addEventListener("close", function() {
	//clean data binding to prevent memory leaks
});
$.homescreen.addEventListener("android:back", function() {
	//"home" button functionality. Leave app runnning in background.
	var intent = Ti.Android.createIntent({
        action: Ti.Android.ACTION_MAIN
    });
    intent.addCategory(Ti.Android.CATEGORY_HOME);
    Ti.Android.currentActivity.startActivity(intent);
});